package cn.mrlong.basicframework.utils.threadmanager;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AppExecutors {
    private static final String TAG = "AppExecutors";
    private static int corePoolSize = Runtime.getRuntime().availableProcessors() * 2 + 1;//核心线程池的数量，同时能够执行的线程数量
    private static int maximumPoolSize = corePoolSize;//最大线程池数量，表示当缓冲队列满的时候能继续容纳的等待任务的数量
    /**
     * 磁盘IO线程池
     **/
    private final ExecutorService diskIO;
    /**
     * 网络IO线程池
     **/
    private final ExecutorService networkIO;
    /**
     * UI线程
     **/
    private final Executor mainThread;
    /**
     * 定时任务线程池
     **/
    private final ScheduledExecutorService scheduledExecutor;

    private volatile static AppExecutors appExecutors;

    public static AppExecutors getInstance() {
        if (appExecutors == null) {
            synchronized (AppExecutors.class) {
                if (appExecutors == null) {
                    appExecutors = new AppExecutors();
                }
            }
        }
        return appExecutors;
    }

    public AppExecutors(ExecutorService diskIO, ExecutorService networkIO, Executor mainThread, ScheduledExecutorService scheduledExecutor) {
        this.diskIO = diskIO;
        this.networkIO = networkIO;
        this.mainThread = mainThread;
        this.scheduledExecutor = scheduledExecutor;
    }

    private AppExecutors() {
        this(diskIoExecutor(), networkExecutor(), new MainThreadExecutor(), scheduledThreadPoolExecutor());
    }

    /**
     * 定时(延时)任务线程池
     * <p>
     * 替代Timer,执行定时任务,延时任务
     */
    public ScheduledExecutorService scheduledExecutor() {
        return scheduledExecutor;
    }

    /**
     * 磁盘IO线程池（单线程）
     * <p>
     * 和磁盘操作有关的进行使用此线程(如读写数据库,读写文件)
     * 禁止延迟,避免等待
     * 此线程不用考虑同步问题
     */
    public ExecutorService diskIO() {
        return diskIO;
    }

    /**
     * 网络IO线程池
     * <p>
     * 网络请求,异步任务等适用此线程
     * 不建议在这个线程 sleep 或者 wait
     */
    public ExecutorService networkIO() {
        return networkIO;
    }

    /**
     * UI线程
     * <p>
     * Android 的MainThread
     * UI线程不能做的事情这个都不能做
     */
    public Executor mainThread() {
        return mainThread;
    }

    private static ScheduledExecutorService scheduledThreadPoolExecutor() {
        return new ScheduledThreadPoolExecutor(corePoolSize,
                Executors.defaultThreadFactory(),//创建线程的工厂
                new ThreadPoolExecutor.AbortPolicy()); //用来对超出maximumPoolSize的任务的处理策略;
    }

    private static ExecutorService diskIoExecutor() {
        return new ThreadPoolExecutor(
                1,
                1,
                0L,//表示的是maximumPoolSize当中等待任务的存活时间
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),//缓冲队列，用于存放等待任务，Linked的先进先出
                Executors.defaultThreadFactory(),//创建线程的工厂
                new ThreadPoolExecutor.AbortPolicy()); //用来对超出maximumPoolSize的任务的处理策略;
    }

    private static ExecutorService networkExecutor() {
        return new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                2500,//表示的是maximumPoolSize当中等待任务的存活时间
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),//缓冲队列，用于存放等待任务，Linked的先进先出
                Executors.defaultThreadFactory(),//创建线程的工厂
                new ThreadPoolExecutor.AbortPolicy()); //用来对超出maximumPoolSize的任务的处理策略;
    }


    private static class MainThreadExecutor implements Executor {
        private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable command) {
            mainThreadHandler.post(command);
        }
    }
}

