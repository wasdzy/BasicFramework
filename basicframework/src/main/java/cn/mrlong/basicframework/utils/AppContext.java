package cn.mrlong.basicframework.utils;

import java.util.HashMap;
import java.util.Map;

public class AppContext {
	public static final String SESSION_ID = "session_id";

	private static AppContext instance;
	private Map<Object, Object> map;

	private Map<Object, Object> getMap() {
		return map;
	}

	private void setMap(Map<Object, Object> map) {
		this.map = map;
	}

	private AppContext() {

	}

	public static AppContext getInstance() {
		if (instance == null) {
			instance = new AppContext();
			instance.setMap(new HashMap<Object, Object>());
		}

		return instance;
	}

	public void putNewObj(Object key, Object value) {
		remove(key);
		put(key, value);
	}

	public void put(Object key, Object value) {
		map.put(key, value);
	}

	public Object get(Object key) {
		return map.get(key);
	}

	public Object remove(Object key) {
		Object object = map.remove(key);
		return object;
	}

	public void clear() {
		map.clear();
	}
}
