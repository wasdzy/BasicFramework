package cn.mrlong.basicframework.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import cn.mrlong.basicframework.R;
import cn.mrlong.basicframework.utils.Res;

/**
 * Created by ly343 on 2019/8/6.
 * 优化Toast，功能防止用户短时间内多次触发Toast而发生的Toast泛滥
 */
@SuppressLint("ShowToast")
public class ToastUtil {
    private static Toast toast = null;

    private ToastUtil() {
    }

    public static void showMsg(Context context, String text) {
        toast = getInstance(context, Gravity.BOTTOM, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void showCenterMsg(Context context, String text) {
        toast = getInstance(context, Gravity.CENTER, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void showMsg(Context context, String text, int gravity) {
        toast = getInstance(context, gravity, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    private static Toast getInstance(Context context, int gravity, String msg, int during) {
        if (toast == null) {
            toast = Toast.makeText(context, msg, during);
            // // 加入个人偏好布局设置
            //toast.getView().setBackgroundResource(R.drawable.toast_bg);
            toast.getView().setBackgroundColor(Res.getColorRes(R.color.color4, context));
            TextView textView = (TextView) toast.getView().findViewById(android.R.id.message);
            textView.setTextSize(15);
            textView.setPadding(25, 0, 25, 0);
            textView.setTextColor(Color.WHITE);
        }
        toast.setGravity(gravity, 0, 0);
        toast.setDuration(during);
        toast.setText(msg);
        return toast;
    }

    public static void showMessage(final Context context, final String msg) {
        toast = getInstance(context, Gravity.CENTER, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * 连续展示，仅改变原生的样式
     *
     * @param context
     * @param msg
     * @param during
     */
    public static void showOrderMessage(final Context context, final int msg, final int during) {
        Toast myToast = Toast.makeText(context, msg, during);
        //myToast.getView().setBackgroundResource(R.drawable.toast_bg);
        TextView textView = (TextView) myToast.getView().findViewById(android.R.id.message);
        textView.setTextSize(15);
        textView.setPadding(5, 5, 5, 5);
        textView.setTextColor(Color.WHITE);
        myToast.setDuration(during);
        myToast.setText(msg);
        myToast.show();
    }

    /**
     * 连续展示，仅改变原生的样式
     *
     * @param context
     * @param msg
     * @param during
     */
    public static void showOrderMessage(final Context context, final String msg, final int during) {
        Toast myToast = Toast.makeText(context, msg, during);
        //myToast.getView().setBackgroundResource(R.drawable.toast_bg);
        TextView textView = (TextView) myToast.getView().findViewById(android.R.id.message);
        textView.setTextSize(15);
        textView.setPadding(5, 5, 5, 5);
        textView.setTextColor(Color.WHITE);
        myToast.setDuration(during);
        myToast.setText(msg);
        myToast.show();
    }
}
