package cn.mrlong.basicframework.utils;

public class StaticConstUtils {

    public static final int DOUBLE_CLICK_EXIT_DENY = 2000;//双击间隔500ms以内退出应用
    public static final float FONT_SIZE_VALUE = 1f;//默认不放大
    public static final String FONT_SIZE_VALUE_SP = "FONT_SIZE_VALUE_SP";//缓存中key名
    public static final String FONT_SIZE_FILE = "FONT_SIZE_FILE";//缓存字体大小文件名

}
