package cn.mrlong.basicframework.utils.l2cache;


import android.content.Context;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 保存全局对象的单例 参考实例
 */
public class SaveInstance implements Serializable, Cloneable {
    public static String PATH = "";
    public final static String TAG = "SaveInstance";

    private static SaveInstance instance;

    public static void getCacheFilePath(Context context) {
        PATH = context.getCacheDir().getAbsolutePath() + "/";
    }

    public static SaveInstance getInstance() {
        if (null == instance) {
            Object obj = StreamUtil.restoreObject(PATH + TAG);
            if (null == obj) {
                obj = new SaveInstance();
                StreamUtil.saveObject(PATH + TAG, obj);
            }
            instance = (SaveInstance) obj;
        }
        return instance;
    }

    private UserInfoBean userInfo;
    private String title;
    private HashMap<String, Object> map;

    public UserInfoBean getUserInfo() {
        return userInfo;
    }

    public String getTitle() {
        return title;
    }

    public HashMap<String, Object> getMap() {
        return map;
    }

    /**
     * 是否需要保存到本地
     */
    public void setUserInfo(UserInfoBean userInfo, boolean needSave) {
        this.userInfo = userInfo;
        if (needSave) {
            save();
        }
    }

    public void setTitle(String title, boolean needSave) {
        this.title = title;
        if (needSave) {
            save();
        }
    }

    /**
     * 把不支持序列化的对象转换成String类型存储
     */
    public void setMap(HashMap<String, Object> map, boolean needSave) {
        this.map = new HashMap<String, Object>();
        if (null == map) {
            StreamUtil.saveObject(PATH + TAG, this);
            return;
        }
        Set set = map.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            this.map.put(String.valueOf(entry.getKey()),
                    String.valueOf(entry.getValue()));
        }
        if (needSave) {
            save();
        }
    }

    private void save() {
        StreamUtil.saveObject(PATH + TAG, this);
    }

    // App退出的时候，清空本地存储的对象，否则下次使用的时候还会存有上次遗留的数据
    public void reset() {
        this.userInfo = null;
        this.title = null;
        this.map = null;
        save();
    }

    // -----------以下3个方法用于序列化-----------------
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    // 保证单例序列化后不产生新对象
    public SaveInstance readResolve() throws ObjectStreamException,
            CloneNotSupportedException {
        instance = (SaveInstance) this.clone();
        return instance;
    }

    private void readObject(ObjectInputStream ois) throws IOException,
            ClassNotFoundException {
        ois.defaultReadObject();
    }
}
