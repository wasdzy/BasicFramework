package cn.mrlong.basicframework.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BINGO on 2018/07/13.
 */

public class ProjectPathUtil {
    public boolean createMVP(String packageName, String activityName) {
        if (packageName.length() == 0) {
            return false;
        }
        try {
            packageName = packageName.replace(".", "/");
            String rootUrl = "app/src/main/java/" + packageName;
            List<String> pathList = new ArrayList<>();
            pathList.add("/ui/main/iview/I" + activityName + "View.java");
            pathList.add("/ui/main/ipresenter/I" + activityName + "Presenter.java");
            pathList.add("/ui/main/presenter/" + activityName + "Presenter.java");
            for (String path : pathList) {
                File file = new File(rootUrl + path);
                if (!file.exists()) {
                    file.createNewFile();
                    String data = ("package " + packageName + path + ";").replace("/", ".")
                            .replace(file.getName(), "");
                    data = data.substring(0, data.length() - 2) + ";";
                    String classType = "class";
                    if (path.contains("ipresenter/I") || path.contains("iview/I")) {
                        classType = "interface";
                    }
                    String datae = "";
                    if("interface".equals(classType)){
                        datae = data + "\n\npublic " + classType + " " + file.getName().replace(".java", "") + "{\n}";
                    }else{
                        datae = data + "\n\npublic " + classType + " " + file.getName().replace(".java", "") +" implements I"+file.getName().replace(".java", "")+""+ "{\n}";
                    }
                    System.out.println(datae);
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(datae);
                    bw.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean creatFilePath(String packageName, boolean isGreendao) {

        if (packageName.length() == 0) {
            return false;
        }
        packageName = packageName.replace(".", "/");
        String rootUrl = "app/src/main/java/" + packageName;

        List<String> pathList = new ArrayList<>();
        pathList.add("/utils");
        if (isGreendao)
            pathList.add("/greendao");
        pathList.add("/ui");
        pathList.add("/ui/bean");
        pathList.add("/ui/main");
        pathList.add("/ui/main/adapter");
        pathList.add("/ui/main/ipresenter");
        pathList.add("/ui/main/presenter");
        pathList.add("/ui/main/iview");
        pathList.add("/ui/main/view");
        pathList.add("/ui/receiver");
        pathList.add("/ui/service");
        for (String path : pathList) {
            File file = new File(rootUrl + path);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return true;
    }


}
