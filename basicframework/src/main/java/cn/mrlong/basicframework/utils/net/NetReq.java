package cn.mrlong.basicframework.utils.net;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.util.Log;

import org.xutils.common.Callback;
import org.xutils.common.util.KeyValue;
import org.xutils.http.RequestParams;
import org.xutils.x;

import cn.mrlong.basicframework.utils.NetUtils;
import cn.mrlong.basicframework.utils.StringUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by BINGO on 2017/08/24.
 */

public class NetReq {
    private static int connectTimeOut = 10 * 1000;

    /**
     * restfull 提交post
     *
     * @param context        用于检测网络
     * @param connectTimeOut 设置超时
     * @param action
     * @param json
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(Context context, int connectTimeOut, String action, String json, final HttpCallback callback) {
        if (null != context && !NetUtils.isNetworkAvailable(context)) {
            callback.onFailure(new NetworkErrorException("网络连接错误"));
        }
        Observable.just("")
                .subscribeOn(Schedulers.io())//子线程查询数据
                .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String res) throws Exception {
                        callback.onStart();
                    }
                });

        final RequestParams entity = new RequestParams(action);
        entity.setAsJsonContent(true);
        entity.setBodyContent(json);
        Log.e("xutils=>", "---------Start---------");
        Log.e("xutils=>", "URL: " + entity.getUri() + ":参数:" + json);
        if (entity != null) {
            entity.setConnectTimeout(connectTimeOut);
        }
        return x.http().post(entity, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Observable.just(result)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                callback.onSuccess(s);
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + s);
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Observable.just(ex)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<Throwable>() {
                            @Override
                            public void accept(@NonNull Throwable e) throws Exception {
                                callback.onFailure(e);
                                e.printStackTrace();
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + e.getMessage());
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callback.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                Observable.just("")
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(@NonNull String res) throws Exception {
                                callback.onFinish();
                            }
                        });
            }
        });

    }

    /**
     * restfull 提交post
     *
     * @param context  用于检测网络
     * @param action
     * @param json
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(Context context, String action, String json, final HttpCallback callback) {
        return post(context, connectTimeOut, action, json, callback);
    }

    /**
     * restfull 提交post
     *
     * @param action
     * @param json
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(String action, String json, final HttpCallback callback) {
        return post(null, connectTimeOut, action, json, callback);
    }

    /**
     * form表单提交方式post
     *
     * @param context        用于检测网络
     * @param connectTimeOut 设置超时
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(Context context, int connectTimeOut, final RequestParams entity, final HttpCallback callback) {
        if (null != context && !NetUtils.isNetworkAvailable(context)) {
            callback.onFailure(new NetworkErrorException("网络连接错误"));
        }
        Observable.just("")
                .subscribeOn(Schedulers.io())//子线程查询数据
                .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String res) throws Exception {
                        callback.onStart();
                    }
                });
        String param = "";
        if (entity != null) {
            if (entity.getStringParams().size() > 0) {
                for (KeyValue keyValue : entity.getStringParams()) {
                    param += "&" + keyValue.key + "=" + keyValue.value;
                }
            }
            if (StringUtils.isNotBlank(param)) {
                param = param.replaceFirst("&", "?");
            }
        }
        Log.e("xutils=>", "---------Start---------");
        Log.e("xutils=>", "URL: " + entity.getUri() + ":参数:" + param);
        if (entity != null) {
            entity.setConnectTimeout(connectTimeOut);
        }
        return x.http().post(entity, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Observable.just(result)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                callback.onSuccess(s);
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + s);
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Observable.just(ex)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<Throwable>() {
                            @Override
                            public void accept(@NonNull Throwable e) throws Exception {
                                callback.onFailure(e);
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + e.getMessage());
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callback.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                Observable.just("")
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(@NonNull String res) throws Exception {
                                callback.onFinish();
                            }
                        });
            }
        });
    }

    /**
     * form表单提交方式post
     *
     * @param context
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(Context context, final RequestParams entity, final HttpCallback callback) {
        return post(context, connectTimeOut, entity, callback);
    }

    /**
     * form表单提交方式post
     *
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable post(final RequestParams entity, final HttpCallback callback) {
        return post(null, connectTimeOut, entity, callback);
    }

    /**
     * form表单提交方式get
     *
     * @param context
     * @param connectTimeOut
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable get(Context context, int connectTimeOut, final RequestParams entity, final HttpCallback callback) {
        if (null != context && !NetUtils.isNetworkAvailable(context)) {
            callback.onFailure(new NetworkErrorException("网络连接错误"));
        }
        Observable.just("")
                .subscribeOn(Schedulers.io())//子线程查询数据
                .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String res) throws Exception {
                        callback.onStart();
                    }
                });
        String param = "";
        if (entity != null) {
            if (entity.getStringParams().size() > 0) {
                for (KeyValue keyValue : entity.getStringParams()) {
                    param += "&" + keyValue.key + "=" + keyValue.value;
                }
            }
            if (StringUtils.isNotBlank(param)) {
                param = param.replaceFirst("&", "?");
            }
        }
        Log.e("xutils=>", "---------Start---------");
        Log.e("xutils=>", "URL: " + entity.getUri() + ":参数:" + param);
        if (entity != null) {
            entity.setConnectTimeout(connectTimeOut);
        }
        return x.http().get(entity, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Observable.just(result)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                callback.onSuccess(s);
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + s);
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Observable.just(ex)
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<Throwable>() {
                            @Override
                            public void accept(@NonNull Throwable e) throws Exception {
                                callback.onFailure(e);
                                Log.e("xutils=>", "URL: " + entity.getUri() + ":返回:" + e.getMessage());
                                Log.e("xutils=>", "----------End----------");
                            }
                        });
            }

            @Override
            public void onCancelled(CancelledException cex) {
                callback.onCancelled(cex);
            }

            @Override
            public void onFinished() {
                Observable.just("")
                        .subscribeOn(Schedulers.io())//子线程查询数据
                        .observeOn(AndroidSchedulers.mainThread())//主线程更新ui
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(@NonNull String res) throws Exception {
                                callback.onFinish();
                            }
                        });
            }
        });
    }

    /**
     * form表单提交方式get
     *
     * @param context
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable get(Context context, final RequestParams entity, final HttpCallback callback) {
        return get(context, connectTimeOut, entity, callback);
    }

    /**
     * form表单提交方式get
     *
     * @param entity
     * @param callback
     * @return
     */
    public static Callback.Cancelable get(final RequestParams entity, final HttpCallback callback) {
        return get(null, connectTimeOut, entity, callback);
    }
}