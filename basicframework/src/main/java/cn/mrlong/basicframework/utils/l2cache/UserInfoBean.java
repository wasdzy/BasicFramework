package cn.mrlong.basicframework.utils.l2cache;

import java.io.Serializable;

/**
 * 用户信息 参考实例
 */
public class UserInfoBean implements Serializable {
    public static final String TAG = "UserInfoBean";
    private static final long serialVersionUID = 1L;
    private String name;
    private String tel;
    private String token;
    private static final String PATH = SaveInstance.PATH;

    public UserInfoBean(String name, String tel, String token) {
        super();
        this.name = name;
        this.tel = tel;
        this.token = token;
        // save();
    }

    private void save() {
        StreamUtil.saveObject(PATH + TAG, this);
    }

    // App退出的时候，清空本地存储的对象，否则下次使用的时候还会存有上次遗留的数据
    public void reset() {
        this.name = null;
        this.tel = null;
        this.token = null;
        // save();
    }

    @Override
    public String toString() {
        return "UserInfoBean [name=" + name + ", tel=" + tel + ", token="
                + token + "]";
    }

    public void setName(String name) {
        this.name = name;
        // save();
    }

    public void setTel(String tel) {
        this.tel = tel;
        // save();
    }

    public void setToken(String token) {
        this.token = token;
        // save();
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public String getToken() {
        return token;
    }

}
