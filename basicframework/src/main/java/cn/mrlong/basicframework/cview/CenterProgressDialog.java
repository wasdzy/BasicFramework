package cn.mrlong.basicframework.cview;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import cn.mrlong.basicframework.R;

/**
 * Created by BINGO on 2017/05/18.
 */

public class CenterProgressDialog extends ProgressDialog {
    private String content;
    private TextView tvCancel;
    private CancelButtonOnClickListener cancelButtonCallBack;

    public interface CancelButtonOnClickListener {
        void cancel();
    }

    public CenterProgressDialog(Context context, String content, int theme) {
        super(context, theme);
        this.content = content;
    }

    public CenterProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public CenterProgressDialog(Context context, String content, int theme, Boolean b) {
        super(context, theme);
        this.content = content;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init(getContext());
    }

    public Context context;

    private void init(Context context) {
        this.context = context;
        //设置不可取消，点击其他区域不能取消，实际中可以抽出去封装供外包设置
        setCancelable(cancelAbleBack);
        setCanceledOnTouchOutside(onTouchOutsideCancel);
        setContentView(R.layout.dialog_progress_layout);
        //设置内容
        TextView textView = (TextView) findViewById(R.id.tv_hint);
        if (content.length() != 0 && !"".equals(context)) {
            textView.setText(content);
            textView.setVisibility(View.VISIBLE);
        }
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        if (null != tvCancel && null != cancelButtonCallBack) {
            tvCancel.setVisibility(View.VISIBLE);
            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != cancelButtonCallBack)
                        cancelButtonCallBack.cancel();
                }
            });
        } else {
            if (null != tvCancel) tvCancel.setVisibility(View.GONE);
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.dimAmount = 0.5f;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }


    public void setCancelButtonCallBack(final CancelButtonOnClickListener cancelButtonCallBack) {
        this.cancelButtonCallBack = cancelButtonCallBack;
    }

    private boolean cancelAbleBack = false;

    public void setCancel(boolean cancelAbleBack) {
        this.cancelAbleBack = cancelAbleBack;
    }

    private boolean onTouchOutsideCancel = false;

    public void setCanceledOnTouchOutsideAble(boolean onTouchOutsideCancel) {
        this.onTouchOutsideCancel = onTouchOutsideCancel;
    }

    @Override
    public void show() {
        if (null != context && !((Activity) context).isFinishing()) super.show();
    }

    @Override
    public void dismiss() {
        if (null != context && !((Activity) context).isFinishing()) super.dismiss();
    }


}