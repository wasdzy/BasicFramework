package cn.mrlong.basicframework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import cn.mrlong.basicframework.utils.threadmanager.AppExecutors;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //UI线程
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                //do something
            }
        });
        //磁盘IO线程池
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                //do something
            }
        });
        //网络IO线程池
        AppExecutors.getInstance().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                //do something
            }
        });
        //延时3秒后执行
        AppExecutors.getInstance().scheduledExecutor().schedule(new Runnable() {
            @Override
            public void run() {
                // do something
            }
        }, 3, TimeUnit.SECONDS);
        //秒后启动第一次,每3秒执行一次(第一次开始执行和第二次开始执行之间间隔3秒)
        AppExecutors.getInstance().scheduledExecutor().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // do something
            }
        }, 5, 3, TimeUnit.MILLISECONDS);
        //秒后启动第一次,每3秒执行一次(第一次执行完成和第二次开始之间间隔3秒)
        ScheduledFuture<?> scheduledFuture = AppExecutors.getInstance().scheduledExecutor().scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                // do something
            }
        }, 5, 3, TimeUnit.MILLISECONDS);
        //取消定时器(等待当前任务结束后，取消定时器)

        scheduledFuture.cancel(false);

    }
}
