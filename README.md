# BasicFramework

Step 1.在项目的 build.gradle 中添加:

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
	
Step 2. 引入

	dependencies {
	        implementation 'com.gitee.wasdzy:BasicFramework:2.1.0.8'
	}
	
Step 3. 打开dataBinding
	
	dataBinding {
        	enabled = true
    	}
Step 4. 依赖引入

    implementation 'com.trello:rxlifecycle:1.0'
    implementation 'com.trello:rxlifecycle-android:1.0'
    implementation 'com.trello:rxlifecycle-components:1.0'
    implementation 'io.reactivex.rxjava2:rxjava:+'
    implementation 'com.alibaba:fastjson:1.1.71.android'
    implementation 'io.reactivex.rxjava2:rxandroid:+'
    implementation 'com.orhanobut:logger:1.15'
    implementation 'org.greenrobot:eventbus:3.0.0'
    implementation 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    implementation 'com.tbruyelle.rxpermissions2:rxpermissions:0.9.5@aar'
    implementation 'com.tencent.bugly:crashreport:latest.release'
    implementation 'com.tencent.bugly:nativecrashreport:latest.release'
    //implementation 'com.android.support:multidex:1.0.3'
    implementation 'org.xutils:xutils:3.5.0'
